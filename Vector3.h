#pragma once
class Vector3
{
private:
	int x, y, z;
public:
	Vector3(double X, double Y, double Z);
	double getMagnitude();
	void printVector();
};

