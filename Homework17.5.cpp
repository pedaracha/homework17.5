﻿#include "Vector3.h"
#include <iostream>

int main()
{
    Vector3 vector(0, 3, 4);
    vector.printVector();
    std::cout <<"vector.getMagnitude(): "<< vector.getMagnitude() << std::endl;
}
