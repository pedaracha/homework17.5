#include "Vector3.h"
#include <math.h>
#include <iostream>

Vector3::Vector3(double X, double Y, double Z)
{
	x = X;
	y = Y;
	z = Z;
}

double Vector3::getMagnitude() 
{
	return sqrt(x * x + y * y + z * z);
}

void Vector3::printVector()
{
	std::cout << x << ',' << y << ',' << z<<std::endl;
}
